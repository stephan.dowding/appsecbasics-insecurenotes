import React, { useState, useEffect } from 'react';

const useStateWithSessionStorage = (sessionStorageKey, defaultValue) => {
  const [value, setValue] = React.useState(
    sessionStorage.getItem(sessionStorageKey) || defaultValue
  );

  React.useEffect(() => {
    if (value) sessionStorage.setItem(sessionStorageKey, value)
    else sessionStorage.removeItem(sessionStorageKey);
  }, [sessionStorageKey, value]);

  return [value, setValue];
};

function Login(props) {
  // Declare a new state variable, which we'll call "count"
  const [userToken, setUserToken] = useStateWithSessionStorage("userToken", null);
  const [username, setUsername] = useStateWithSessionStorage("userName", '');
  const [password, setPassword] = useState('');
  const [showError, setShowError] = useState(false);
  const [showPassword, setShowPassword] = useState(false);

  useEffect(() => {
    props.SetUserToken(userToken);
  });

  const logout = () => {
      setUserToken(null);
      setUsername("");
      setPassword("");
      setShowError(false);
  }
  
  const attemptLogin = async () => {
      const payload = {
        username,
        password
    }
    setShowError(false);
    const response = await fetch("/login", {
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
          },
        body: JSON.stringify(payload)
    });
    if (!response.ok) {
        setShowError(true);
        return;
    }
    const data = await response.json();
    setUserToken(data.token);
    setUsername(data.username);
    setPassword("");
  }

  if (userToken === null) {
    return (
        <div>
          <p>Please log in</p>
          <input type="text" placeholder="username" onChange={e => setUsername(e.target.value)}></input>
          <input type={showPassword?"text":"password"} placeholder="password" onChange={e => setPassword(e.target.value)}></input>
          <label><input type="checkbox" checked={showPassword} onChange={e => setShowPassword(e.target.checked)}></input>Show Password</label>
          <br/>
          <button onClick={attemptLogin}>Login</button>
          { showError? <text>Nope!</text> : <></>}
        </div>
      );
  }
  return (
    <p>Welcome {username}! <button onClick={logout}>Logout</button></p>
  );
}

export default Login;