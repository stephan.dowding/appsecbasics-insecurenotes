import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

function NoteList() {
    const [notes, setNotes] = useState(null);
    const [error, setError] = useState(false);
    const [fetching, setFetching] = useState(false);

    useEffect(() => {
        const getData = async () => {
            setFetching(true);
            const response = await fetch("/notes", {
                method: "GET",
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${sessionStorage.getItem("userToken")}`
                }
            });
            if (!response.ok) {
                setError(true);
                return;
            }
            const data = await response.json();
            setNotes(data);
        }
        if (!fetching) {
            getData();
        }
    });

    const showNotes = () => {
        if (error)
            return <p>Uh oh! Something went wrong!</p>

        if (!notes) 
            return <p>Loading...</p>

        if (!notes.length)
            return <p>You have not created any notes</p>

        return <>
            <p>Your notes are:</p>
            <hr />
            {notes.map(note => {
                return <div key={note._id}>
                    <h3>{note.title} - <Link to={`/notes/${note._id}`}>Edit</Link></h3>
                    <div dangerouslySetInnerHTML={{__html: note.note}}></div>
                    <hr />
                </div>
            })}
            </>
    }

    return <>
        <p><Link to="/notes/create">Create a new note</Link></p>
        {showNotes()}
    </>
}

export default NoteList