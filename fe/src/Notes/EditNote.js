import React, { useState, useEffect } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';

function EditNote() {

    const [fetching, setFetching] = useState(false);
    const [title, setTitle] = useState('');
    const [note, setNote] = useState('');
    const [noteObject, setNoteObject] = useState(null);
    const [error, setError] = useState(false);
    const [showError, setShowError] = useState(false);
    const navigate = useNavigate();

    const params = useParams();

    useEffect(() => {
        const getData = async () => {
            setFetching(true);
            const response = await fetch(`/notes/${params.id}`, {
                method: "GET",
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${sessionStorage.getItem("userToken")}`
                }
            });
            if (!response.ok) {
                setError(true);
                return;
            }
            const data = await response.json();
            setNoteObject(data);
            setTitle(data.title);
            setNote(data.note);
        }
        if (!fetching) {
            getData();
        }
    });

    const saveNote = async () => {
        setShowError(false);
        const payload = { ...noteObject, title, note };
        const response = await fetch(`/notes/${params.id}`, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${sessionStorage.getItem("userToken")}`
              },
            body: JSON.stringify(payload)
        });
        if (!response.ok) {
            setShowError(true);
            return;
        }
        navigate("/notes");

    }

    const showNote = () => {
        if (error)
            return <p>Uh oh! Something went wrong!</p>

        if (!note) 
            return <p>Loading...</p>

        return <>
            <p>Title: <input type="text" onChange={e => setTitle(e.target.value)} value={title}></input></p>
            <p>Note:</p><textarea onChange={e => setNote(e.target.value)} value={note}></textarea>
            <p><button onClick={saveNote}>Save</button></p>
            {showError? <p>Something went wrong</p> : <></>}
            </>
    }

    return <>
            <p><Link to="/notes">Cancel</Link></p>
            {showNote()}
        </>
}

export default EditNote;