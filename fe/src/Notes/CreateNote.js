import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';

function CreateNote() {

    const [title, setTitle] = useState('');
    const [note, setNote] = useState('');
    const [showError, setShowError] = useState(false);
    const navigate = useNavigate();


    const saveNote = async () => {
        setShowError(false);
        const payload = { title, note };
        const response = await fetch("/notes", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${sessionStorage.getItem("userToken")}`
              },
            body: JSON.stringify(payload)
        });
        if (!response.ok) {
            setShowError(true);
            return;
        }
        navigate("/notes");

    }

    return <>
        <Link to="/notes">Cancel</Link>
        <p>Title: <input type="text" onChange={e => setTitle(e.target.value)}></input></p>
        <p>Note:</p><textarea onChange={e => setNote(e.target.value)}></textarea>
        <p><button onClick={saveNote}>Save</button></p>
        {showError? <p>Something went wrong</p> : <></>}
    </>
}

export default CreateNote;