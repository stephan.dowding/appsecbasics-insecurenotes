import React from 'react';
import ReactDOM from 'react-dom';
import {
  BrowserRouter,
  Routes,
  Route,
  Navigate
} from "react-router-dom";
import './index.css';
import App from './App';
import NoteList from './Notes/NoteList';
import CreateNote from './Notes/CreateNote';
import EditNote from './Notes/EditNote';

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<App />}>
          <Route index element={<NoteList />} />
          <Route path="notes" element={<NoteList />} />
          <Route path="notes/create" element={<CreateNote />} />
          <Route path="notes/:id" element={<EditNote />} />
        </Route>
        <Route path="*" element={<Navigate to="/" />} />
      </Routes>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
);
