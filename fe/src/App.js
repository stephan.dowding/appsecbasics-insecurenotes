import './App.css';
import Login from './Login';
import { useState } from 'react';
import { Outlet } from "react-router-dom";

function App() {

  const [userToken, setUserToken] = useState(null);

  const renderItemList = () => {
    if (userToken) {
      return <Outlet></Outlet>
      //<NoteList userToken={userToken}></NoteList>
    } else {
      return <p>Please login to view your Notes!</p>
    }
  }

  return (
    <div className="App">
      <Login SetUserToken={setUserToken}></Login>
      {renderItemList()}
    </div>
  );
}

export default App;
