#!/bin/bash

cd fe
npx yarn start & fepid=$!
cd ../be
npx yarn start & bepid=$!
cd ..
trap "kill $fepid $bepeid" SIGINT
wait $fepid $bepeid