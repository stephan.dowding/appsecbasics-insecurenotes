import fastify from 'fastify';
import fastifyJwt from 'fastify-jwt';
import { MongoMemoryServer } from 'mongodb-memory-server';
import { MongoClient, ObjectId } from 'mongodb';

const main = async () => {
  const mongod = await MongoMemoryServer.create();
  const client = new MongoClient(mongod.getUri());
  console.log(`Mongo Uri : ${mongod.getUri()}`);

  await client.connect();
  const db = client.db('dbName');
  const usersCollection = db.collection('users');
  const notesCollection = db.collection('notes');

  const insertResult = await usersCollection.insertMany([
    {username: "Alice", password: "CorrectHorseBatteryStaple" },
    { username: "Bob", password: "WhatPassword?" },
    { username: "Admin", password: "Admin123!" }]);
  console.log('Inserted documents =>', insertResult);

  const server = fastify();

  server.register(fastifyJwt, {
    secret: 'squirrel'
  });

  server.decorate("authenticate", async function(request, reply) {
    try {
      await request.jwtVerify();
    } catch (err) {
      reply.send(err);
    }
  })
    
  server.post(
    '/login',
    async (request, reply) => {

      const doc = await usersCollection.findOne(
        request.body
      );

      if (doc) {
        const token = server.jwt.sign({ 
          sub: doc._id,
          name: doc.username 
        });
        reply.send({ username: doc.username, token });
      } else {
        reply.status(404);
        reply.send();
      }
    },
  );

  server.get('/ping', async (request, reply) => {
    return 'pong\n'
  });

  server.get('/secureping',
  {
    preValidation: [server.authenticate]
  },
  async (request, reply) => {
    return 'Secure Pong!\n'
  });

  server.get('/notes',
  {
    preValidation: [server.authenticate]
  },
  async (request, reply) => {
    const notes = await notesCollection.find({owner: request.user.sub}).toArray();
    reply.send(notes);
  });

  server.get('/notes/:noteId',
  {
    preValidation: [server.authenticate]
  },
  async (request, reply) => {
    const doc = await notesCollection.findOne({_id: ObjectId(request.params.noteId)});
    if (doc){
      reply.send(doc);
    } else {
      reply.status(404);
      reply.send();
    }
  });

  server.put('/notes/:noteId',
  {
    preValidation: [server.authenticate]
  },
  async (request, reply) => {
    const payload = request.body;
    delete payload._id;
    const doc = await notesCollection.findOneAndReplace(
      {_id: ObjectId(request.params.noteId)},
      payload
    );
    if (doc){
      reply.send(doc);
    } else {
      reply.status(404);
      reply.send();
    }
  });

  server.post('/notes',
  {
    preValidation: [server.authenticate]
  },
  async (request, reply) => {
    const owner = request.user.sub;
    const doc = {...request.body, owner};
    const result = await notesCollection.insertOne(doc);
    reply.send(result);
  });

  const address = await server.listen(8080);
  console.log(`Server listening at ${address}`);
};

main();