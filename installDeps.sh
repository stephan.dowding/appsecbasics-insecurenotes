#!/bin/bash

cd fe
npx yarn & fepid=$!
cd ../be
npx yarn & bepid=$!
cd ..
trap "kill $fepid $bepeid" SIGINT
wait $fepid $bepeid